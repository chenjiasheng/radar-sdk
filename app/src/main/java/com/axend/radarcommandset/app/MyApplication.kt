package com.axend.radarcommandset.app

import android.app.Application
import com.axend.radarcommandsdk.connect.DeviceNetworkConnect

class MyApplication : Application() {


    override fun onCreate() {
        super.onCreate()
        /*初始化*/
        DeviceNetworkConnect.init(this)
    }
}