package com.axend.radarcommandsdk.utils

import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothManager
import android.bluetooth.le.BluetoothLeScanner
import android.content.Context
import com.axend.radarcommandsdk.connect.DeviceNetworkConnect
import com.axend.radarcommandsdk.connect.DeviceNetworkConnect.Companion.applicationContext

/**
 * @description蓝牙工具类
 * @author admins
 * @time 2024/1/11 13:54
 */
class BleUtils private constructor() {

    private var blueToolsAdapter: BluetoothAdapter? =null

    init {
        if (blueToolsAdapter == null) {
            blueToolsAdapter =
                (applicationContext?.getSystemService(Context.BLUETOOTH_SERVICE) as BluetoothManager).adapter
            blueToolsAdapter ?: LogUtil.e("此设备无蓝牙功能！")
        }
    }

    companion object {

        @Volatile
        private var instance: BleUtils? = null


        fun getInstance() = instance ?: synchronized(this) {
            instance ?: BleUtils().also { instance = it }
        }

    }



    /**
     * 获取蓝牙适配器
     * */
    fun getBlueToolsAdapter(): BluetoothAdapter = blueToolsAdapter!!


    /**
     * 蓝牙是否打开
     */
    fun isOpenBle(): Boolean = blueToolsAdapter!!.isEnabled


    /**
     * 获取ble扫描仪
     */
    fun getBleScanner(): BluetoothLeScanner = blueToolsAdapter!!.bluetoothLeScanner


}