package com.axend.radarcommandsdk.connect.contract

import com.axend.radarcommandsdk.connect.bean.BleMsgEntity

interface IBleMsgProcessor {

    /**消息编码*/
    fun write(bleMsgEntity: BleMsgEntity): ByteArray


    /**处理消息*/
    fun processor(bleMsgEntity: BleMsgEntity): BleMsgEntity?
}