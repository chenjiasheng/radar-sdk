package com.axend.radarcommandsdk.connect.ble.handler

import com.axend.radarcommandsdk.connect.bean.BleMsgEntity
import com.axend.radarcommandsdk.connect.bean.BleMsgTag
import com.axend.radarcommandsdk.connect.bean.DeviceType
import com.axend.radarcommandsdk.connect.ble.AbstractBleMsgProcessor
import com.axend.radarcommandsdk.utils.ByteUtils

/**
 * @description重启设备指令
 * @author admins
 * @time 2024/1/25 17:50
 * 重置设备处理器（雷达板） （需要注意设备开头协议的魔术位）
 * 协议： magic(1 bytes) ver(1 bytes) type(1 bytes) cmd(1 bytes) req id(int 4byte)  timeout(short 2byte) content len (int) 标识符 (2 bytes) 数据内容
 */
class BleResetDeviceHandler : AbstractBleMsgProcessor() {

    override fun getMsgTag(): BleMsgTag {
        return BleMsgTag.RESET_DEVICE
    }

    override fun write(bleMsgEntity: BleMsgEntity): ByteArray {
        val radarType: DeviceType? = bleMsgEntity.radarType
        val bytes: ByteArray =
            ByteUtils.hexStringToBytes("130101019B000000102700000006041100000000")

        //Assure雷达不走蓝牙协议
        if (radarType !== DeviceType.ASSURE) {
            bytes[0] = radarType!!.magicHead
        }
        return bytes
    }


    override fun processor(bleMsgEntity: BleMsgEntity): BleMsgEntity? {
        return super.processor(bleMsgEntity)
    }
}