package com.axend.radarcommandsdk.connect.contract

/**
 * @description 设备状态回调
 * @author admins
 * @time 2023/12/19 11:18
 */
interface IDeviceStatusCallback {

    /** 连接状态值 11:成功 12:失败 13:中断*/
    fun callBackDeviceStatus(status: Int)

    /** 设备返回数据 */
    fun callBackDeviceData(message:Any)

}