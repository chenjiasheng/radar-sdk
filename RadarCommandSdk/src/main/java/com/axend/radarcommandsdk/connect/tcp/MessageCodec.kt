package com.axend.radarcommandsdk.connect.tcp

import com.axend.radarcommandsdk.connect.bean.BodyState
import com.axend.radarcommandsdk.connect.bean.TcpMessageEntity
import com.axend.radarcommandsdk.connect.bean.TcpMsgTag
import com.axend.radarcommandsdk.utils.ByteUtils
import com.axend.radarcommandsdk.utils.LogUtil
import io.netty.buffer.ByteBuf
import io.netty.channel.ChannelHandlerContext
import io.netty.handler.codec.MessageToMessageCodec
import java.nio.ByteBuffer

/**
 * @description自定义编解码
 * @author admins
 * @time 2023/12/20 9:38
 */
class MessageCodec : MessageToMessageCodec<ByteBuf, TcpMessageEntity>() {


    /**
     * 消息编码
     */
    override fun encode(ctx: ChannelHandlerContext?, msg: TcpMessageEntity?, out: MutableList<Any>?) {
        val bytes: ByteArray?
        when (msg?.msgTag) {
            TcpMsgTag.getID -> bytes = BodyState.getMessage.getCommandParameter()
            TcpMsgTag.getAlarmDelay -> bytes = BodyState.getMessage.getCommandParameter()
            TcpMsgTag.getDeviceHeight -> bytes = BodyState.getMessage.getCommandParameter()
            TcpMsgTag.getHardwareVersion -> bytes = BodyState.getMessage.getCommandParameter()
            TcpMsgTag.getWorkDistance -> bytes = BodyState.getMessage.getCommandParameter()
            /*服务器ip和端口请用英文逗号分割*/
            TcpMsgTag.setDeviceIPAndPort -> bytes =
                msg.bodyObject.toString().toByteArray()
            /*wifi名称和密码请用英文逗号分割*/
            TcpMsgTag.setFamilyWifiInfo -> bytes =
                msg.bodyObject.toString().toByteArray()

            else -> {
                bytes = msg?.body
            }
        }
        val buf = ctx!!.alloc().buffer()
        //添加头部
        buf.writeBytes(TcpMessageEntity.messageHeader)
        //添加消息长度
        buf.writeBytes(ByteUtils.short2ByteLE(bytes!!.size.toShort()))
        //添加消息标识
        buf.writeBytes(msg?.msgTag?.getRadarCommandByte())
        buf.writeBytes(bytes)

        val b = ByteArray(buf.readableBytes())
        buf.markReaderIndex()
        buf.readBytes(b)
        buf.resetReaderIndex()
        LogUtil.d("发送的原始数据：${ msg?.msgTag} ${ByteUtils.bytesToHexString(b)}" )

        out!!.add(buf)


    }

    override fun decode(ctx: ChannelHandlerContext?, msg: ByteBuf?, out: MutableList<Any>?) {
        LogUtil.d("消息解码")
        val bytes = ByteArray(msg!!.readableBytes())
        msg.markReaderIndex()
        msg.readBytes(bytes)
        msg.resetReaderIndex()
        LogUtil.d("接收原始数据：" + ByteUtils.bytesToHexString(bytes))

        if (msg.readableBytes() < TcpMessageEntity.messageHeader.size
            || !getByteSize(msg, 4).contentEquals(TcpMessageEntity.messageHeader)
        ) {
            LogUtil.d("Message not head")
            return
        }
        val messageEntity = TcpMessageEntity()
        //获取消息标签
        messageEntity.msgTag = TcpMsgTag.get(msg.readShortLE().toInt())
        //跳过长度两个字节
        msg.skipBytes(2)
        var bytesBody = getByteSize(msg, msg.readableBytes())
        messageEntity.length = bytesBody.size
        messageEntity.body = bytesBody
        messageEntity.bodyObject = getBodyObject(messageEntity.msgTag!!, bytesBody)

        out?.add(messageEntity)
    }


    private fun getByteSize(byteBuf: ByteBuf, size: Int): ByteArray {
        return byteBuf.readBytes(size).array()
    }


    /**
     * 消息体字节数组转化成对象
     */
    fun getBodyObject(msgTag: TcpMsgTag, body: ByteArray): Object? {
        val result = when (msgTag) {
            TcpMsgTag.setFamilyWifiInfo -> BodyState.setSuccess.getCommandParameter()
                .contentEquals(body)

            TcpMsgTag.setAlarmDelay -> BodyState.setSuccess.getCommandParameter()
                .contentEquals(body)

            TcpMsgTag.setDeviceHeight -> BodyState.setSuccess.getCommandParameter()
                .contentEquals(body)

            TcpMsgTag.setWorkDistance -> BodyState.setSuccess.getCommandParameter()
                .contentEquals(body)

            TcpMsgTag.setDeviceIPAndPort -> BodyState.setSuccess.getCommandParameter()
                .contentEquals(body)

            TcpMsgTag.getID -> ByteUtils.bytesToHexString(body)
            TcpMsgTag.getAlarmDelay -> ByteUtils.byteToInt(body)
            TcpMsgTag.getWorkDistance -> ByteUtils.byteToFloat(body, 0)
            TcpMsgTag.getHardwareVersion -> getHardwareVersionByString(body)

            else -> {

            }
        }
        return result as Object?
    }


    /**
     * 获取硬件版本信息
     */
    private fun getHardwareVersionByString(bytes: ByteArray): String? {
        LogUtil.d(bytes.toString())
        LogUtil.d(ByteUtils.bytesToHexString(bytes))
        val byteBuffer = ByteBuffer.allocate(bytes.size)
        byteBuffer.put(bytes)
        byteBuffer.rewind()
        var str: String? = null
        //将每个字节转换成Int
        for (i in 0..3) {
            val b: Byte = byteBuffer.get()
            val temp = ByteArray(4)
            temp[3] = b
            //如果是个位数前补零
            val hexString = java.lang.String.format("%02d", ByteUtils.byteToInt(temp))
            if (i == bytes.size - 1) {
                str += hexString
            } else {
                str += "$hexString."
            }
        }
        return str
    }


}