package com.axend.radarcommandsdk.connect.bean

/**
 * @description连接类型
 * @author admins
 * @time 2023/12/18 18:01
*/
enum class DeviceConnectType(type: Int) {
    TYPE_WIFI(1),
    TYPE_BLE(2)

}