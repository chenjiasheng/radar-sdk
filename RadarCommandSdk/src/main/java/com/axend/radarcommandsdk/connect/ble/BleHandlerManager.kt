package com.axend.radarcommandsdk.connect.ble

import com.axend.radarcommandsdk.connect.bean.BleMsgEntity
import com.axend.radarcommandsdk.connect.bean.BleMsgTag
import com.axend.radarcommandsdk.connect.bean.DeviceType
import com.axend.radarcommandsdk.utils.ByteUtils
import com.axend.radarcommandsdk.utils.LogUtil
import java.nio.ByteBuffer
import java.util.concurrent.ConcurrentHashMap

/**
 * @description指令处理集
 * @author admins
 * @time 2024/1/26 17:24
*/
class BleHandlerManager {

    private var bleMsgHandlerMap: ConcurrentHashMap<BleMsgTag, AbstractBleMsgProcessor> =
        ConcurrentHashMap()


    /** 添加处理器  */
    fun addHandler(vararg bleMsgCodecs: AbstractBleMsgProcessor?) {
        if (null == bleMsgCodecs) {
            LogUtil.w("bleMsgCodecs is null")
            return
        }
        for (msgCodec in bleMsgCodecs) {
            addHandler(msgCodec)
        }
    }

    /** 添加处理器  */
    fun addHandler(bleMsgCodec: AbstractBleMsgProcessor?) {
        if (null == bleMsgCodec) {
            LogUtil.w("bleMsgCodec is null")
            return
        }
        bleMsgHandlerMap[bleMsgCodec.getMsgTag()] = bleMsgCodec
    }


    /** 获取处理器  */
    fun getHandler(bleMsgTag: BleMsgTag?): AbstractBleMsgProcessor? {
        return if (null == bleMsgTag) {
            null
        } else bleMsgHandlerMap[bleMsgTag]
    }

    /** 移除处理器  */
    fun removeHandler(bleMsgTag: BleMsgTag?) {
        if (null == bleMsgTag) {
            LogUtil.w("bleMsgTag is null")
            return
        }
        bleMsgHandlerMap.remove(bleMsgTag)
    }

    fun release() {
        if (null == bleMsgHandlerMap || bleMsgHandlerMap.size == 0) {
            return
        }
        bleMsgHandlerMap.clear()
    }

    /**
     * 获取编码后的数据
     * @param bleMsgEntity
     * @return
     */
    fun getWriteBleMsg(bleMsgEntity: BleMsgEntity?): ByteArray? {
        if (null == bleMsgEntity) {
            LogUtil.w("bleMsgEntity is null")
            return ByteArray(0)
        }
        val handler = getHandler(bleMsgEntity.bleMsgTag)
        return handler!!.write(bleMsgEntity)
    }


    /**
     * 处理消息，并找到对应的消息处理器
     * @param data
     */
    fun decode(data: ByteArray?): BleMsgEntity? {
        if (null == data || data.size == 0) {
            LogUtil.w("data is null $data")
            return null
        }
        val dataLength = data.size
        return if (dataLength > 1) {
            //此处为雷达板子协议的数据
            findProcessorHandler(data)
        } else {
            //此处为通讯板子协议的数据
            val handler = getHandler(BleMsgTag.SET_STATUS)
            if (null == handler) {
                LogUtil.w("not found handler")
                return null
            }
            val notifyEntity = BleMsgEntity(BleMsgTag.SET_STATUS, data)
            handler.processor(notifyEntity)
        }
    }


    private fun findProcessorHandler(data: ByteArray): BleMsgEntity? {
        val buffer = ByteBuffer.wrap(data)
        val magic = buffer.get()
        //获取魔术位
        val radarType: DeviceType? = DeviceType.getMagic(magic)
        if (radarType == null) {
            LogUtil.d("magic undefined $data")
        }
        //Assure获取设备id没有协议头 所以radarType会为空
        val position = if (radarType == null) 6 else 14
        //移动到tag位前面
        buffer.position(position)
        val tag = buffer.short
        val bleMsgTag = BleMsgTag.fromTag(tag)
        LogUtil.d(tag="蓝牙返回的数据转换", message = "标题：$tag $bleMsgTag 转换的16进制 ${ByteUtils.bytesToHexString(data)}")
        val handler = getHandler(bleMsgTag)
        if (null == handler) {
            LogUtil.w("not found handler")
            return null
        }

        //截取tag位之后的数据
        val size = buffer.limit() - buffer.position()
        if (size <= 0) {
            LogUtil.w("buffer readable content length is 0")
            return null
        }
        val body = ByteArray(size)
        buffer[body]
        val notifyEntity = BleMsgEntity(bleMsgTag, radarType, body)
        buffer.clear()
        return handler.processor(notifyEntity)
    }

}