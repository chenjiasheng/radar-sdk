package com.axend.radarcommandsdk.connect.contract

import com.axend.radarcommandsdk.connect.bean.DeviceConnectType

/**
 * @description设备连接接口
 * @author admins
 * @time 2023/12/19 11:12
 */
interface IDeviceConnect {

    /**连接*/
    fun connect();

    /** 状态值回调*/
    fun setCallback(statusCallback: IDeviceStatusCallback?)

    /**是否连接 */
    fun isConnect(): Boolean

    /** 关闭连接*/
    fun close()

    /** 发送消息*/
    fun sendMsg(obj: Any?)

    /** 获取连接的类型*/
    fun getConnectType(): DeviceConnectType?

}