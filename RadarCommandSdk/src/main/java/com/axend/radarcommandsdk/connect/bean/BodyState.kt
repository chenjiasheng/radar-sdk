package com.axend.radarcommandsdk.connect.bean

/**
 * @description消息体枚举类
 * @author admins
 * @time 2023/12/19 16:40
 */
enum class BodyState(val state: Int, private val bytes: ByteArray) {
    setSuccess(1, byteArrayOf(0x01, 0x00, 0x00, 0x00)),  //设置设备参数成功
    setFailed(2, byteArrayOf(0x00, 0x00, 0x00, 0x00)),  //设置设备参数失败
    getMessage(3, byteArrayOf(0x00, 0x00, 0x00, 0x00))  ;//获取设备设置的参数


    fun getCommandParameter(): ByteArray = bytes
}