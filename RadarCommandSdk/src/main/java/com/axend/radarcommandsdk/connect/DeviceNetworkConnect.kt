package com.axend.radarcommandsdk.connect

import android.app.Activity
import android.app.Application
import android.content.Context
import android.text.TextUtils
import com.axend.radarcommandsdk.connect.bean.BleMsgEntity
import com.axend.radarcommandsdk.connect.bean.BleMsgTag
import com.axend.radarcommandsdk.connect.bean.DeviceConnectType
import com.axend.radarcommandsdk.connect.bean.DeviceType
import com.axend.radarcommandsdk.connect.bean.TcpMessageEntity
import com.axend.radarcommandsdk.connect.bean.TcpMsgTag
import com.axend.radarcommandsdk.connect.ble.BleClient
import com.axend.radarcommandsdk.connect.contract.IDeviceStatusCallback
import com.axend.radarcommandsdk.constant.STATUS_SUCCESS
import com.axend.radarcommandsdk.constant.STATUS_WIFI_NO_CONNECT
import com.axend.radarcommandsdk.utils.AppExecutors
import com.axend.radarcommandsdk.utils.LogUtil
import com.axend.radarcommandsdk.utils.ToastUtil
import com.axend.radarcommandsdk.utils.WifiUtils
import java.util.concurrent.ConcurrentLinkedQueue
import java.util.concurrent.TimeUnit

/**
 * @description设备配网统一管理
 * @author admins
 * @time 2023/12/19 11:12
 */
class DeviceNetworkConnect private constructor(
    private val deviceType: DeviceType?,
    private val iDeviceStatusCallback: IDeviceStatusCallback?,
    private val radarTcpServer: String?,
    private val wifiSSID: String?,
    private val wifiPassWord: String?
) {
    /**tcp传输队列*/
    private lateinit var tcpConcurrentLinkedQueue: ConcurrentLinkedQueue<TcpMessageEntity>

    /**ble传输队列*/
    private lateinit var bleConcurrentLinkedQueue: ConcurrentLinkedQueue<BleMsgEntity>

    @Volatile
    private var mRun = true


    /**
     * builder模式
     */
    class Builder(private var context: Activity) {
        private var deviceType: DeviceType? = null
        private var iDeviceStatusCallback: IDeviceStatusCallback? = null
        private var radarTcpServer: String? = ""
        private var wifiSSID: String? = ""
        private var wifiPassWord: String? = "NONE"

        /**设置设备类型 */
        fun setDeviceType(deviceType: DeviceType?): Builder {
            this.deviceType = deviceType
            return this
        }

        /**设置设备传输状态回调 */
        fun setDeviceStatusCallback(iDeviceStatusCallback: IDeviceStatusCallback?): Builder {
            this.iDeviceStatusCallback = iDeviceStatusCallback
            return this
        }

        /**设置雷达tcp服务器地址*/
        fun setRadarTcpServer(radarTcpServer: String?): Builder {
            this.radarTcpServer = radarTcpServer
            return this
        }

        /**设置wifi ssid*/
        fun setWifiSSID(wifiSSID: String?): Builder {
            this.wifiSSID = wifiSSID
            return this
        }

        /**设置wifi password*/
        fun setWifiPassWord(wifiPassWord: String?): Builder {
            this.wifiPassWord = wifiPassWord
            return this
        }

        fun build() = createInstance(
            context,
            deviceType,
            iDeviceStatusCallback,
            radarTcpServer,
            wifiSSID,
            wifiPassWord
        )

    }


    companion object {
        private const val TIME_OUT = 12 * 1000
        private const val BLE_SCAN_INTERVAL = 5000L

        /*发送消息队列延迟时间设置*/
        private const val DELAY: Long = 1500

        var context: Activity? = null
        var applicationContext: Application? = null

        @Volatile
        private var mHasInit = false

        /**
         * 非单例方法
         * @param context
         * @param iDeviceType 设备类型
         * @param iDeviceStatusCallback 设备状态回调
         * @param radarTcpServer 雷达tcp服务器地址
         * @param wifiSSID wifi ssid
         * @param wifiPassWord wifi password
         */
        fun createInstance(
            context: Activity?,
            iDeviceType: DeviceType?,
            iDeviceStatusCallback: IDeviceStatusCallback?,
            radarTcpServer: String?,
            wifiSSID: String?,
            wifiPassWord: String?
        ): DeviceNetworkConnect {
            if (!mHasInit) {
                throw RuntimeException("must call DeviceNetworkConnect.init first")
            }

            context?.let {
                Companion.context = it
            }
            return DeviceNetworkConnect(
                iDeviceType,
                iDeviceStatusCallback,
                radarTcpServer,
                wifiSSID,
                wifiPassWord
            )
        }

        /**
         *应用程序初始化时调用
         */
        fun init(application: Application?) {
            application?.let {
                Companion.applicationContext = it
                mHasInit = true
            }
        }

    }


    init {
        deviceType?.let {
            /*设备连接回调监听*/
            it.getDeviceConnectInstance().setCallback(object : IDeviceStatusCallback {
                override fun callBackDeviceStatus(status: Int) {
                    /*如果连接成功则直接发送消息*/
                    if (status == STATUS_SUCCESS) {
                        sendMessageQueue()
                    }
                    /*并且直接返回状态值*/
                    launchUIThread {
                        iDeviceStatusCallback?.callBackDeviceStatus(status)
                    }
                }

                override fun callBackDeviceData(message: Any) {
                    launchUIThread {
                        iDeviceStatusCallback?.callBackDeviceData(message)
                    }


                    if (deviceType.deviceConnectType == DeviceConnectType.TYPE_BLE) {
                        if (TextUtils.isEmpty(message.toString())) {
                            if (linkedQueueIsEmpty()) {
                                stopSend()
                                close()
                                LogUtil.d("命令发送完成，停止发送！")
                            }
                        }
                        /*获取到消息并处理消息队列 蓝牙*/
                        if (message is BleMsgEntity) {
                            when (message.bleMsgTag) {
                                /*处理id*/
                                BleMsgTag.GET_DEVICE_ID -> {
                                    executed()
                                    LogUtil.d("Wavve设备ID:${message.body.toString()}")
                                }

                                BleMsgTag.GET_ASSURE_DEVICE_ID -> {
                                    executed()
                                    LogUtil.d("Assure设备ID:${message.body.toString()}")
                                }

                                BleMsgTag.SET_STATUS -> {
                                    executed(message)
                                    LogUtil.d("设置成功:${message.body}")
                                }

                                else -> {
                                    if (linkedQueueIsEmpty()) {
                                        stopSend()
                                        close()
                                        LogUtil.d("命令发送完成")
                                    }
                                }
                            }
                        }
                    } else {
                        if (message is TcpMessageEntity) {
                            /*处理wifi 消息示例*/
                            when (message.msgTag) {
                                TcpMsgTag.getID -> {
                                    val id: String = message.bodyObject.toString()
                                    LogUtil.d("雷达id：$id")
                                    executed()
                                }

                                TcpMsgTag.getHardwareVersion -> {
                                    val hardwareVersion: String = message.bodyObject.toString()
                                    LogUtil.d("固件版本：$hardwareVersion")
                                    executed()
                                }

                                TcpMsgTag.setFamilyWifiInfo ->                     //设置雷达WIFI账号与密码
                                    if (message.bodyObject as Boolean) {
                                        LogUtil.d("设置wifi账号和密码成功！")
                                        executed()
                                    } else {
                                        LogUtil.d("设置wifi账号和密码失败！")
                                    }

                                TcpMsgTag.setDeviceIPAndPort ->                     //设置雷达IP地址与端口
                                    if (message.bodyObject as Boolean) {
                                        LogUtil.d("设置端口和IP成功！")
                                        executed()
                                    } else {
                                        LogUtil.d("设置雷达IP地址与端口失败")
                                    }

                                else -> {}
                            }
                        }
                        if (linkedQueueIsEmpty()) {
                            stopSend()
                            close()
                            LogUtil.d("命令发送完成，停止发送！")
                        }
                    }

                }

            })
            /*初始化消息队列*/
            if (deviceType.deviceConnectType == DeviceConnectType.TYPE_WIFI) {
                initQueue(
                    TcpMessageEntity(
                        msgTag = TcpMsgTag.setFamilyWifiInfo,
                        bodyObject = "${wifiSSID},${wifiPassWord}" as Object
                    )
                )
            } else {
                initQueue(wifiSSID, wifiPassWord)
            }
        }
    }


    /**初始化Tcp消息队列*/
    private inline fun initQueue(messageEntity: TcpMessageEntity) {
        mRun = true
        tcpConcurrentLinkedQueue = ConcurrentLinkedQueue<TcpMessageEntity>()
        tcpConcurrentLinkedQueue?.let {
            it.clear()
            it.offer(TcpMessageEntity(msgTag = TcpMsgTag.getID))
            it.offer(
                TcpMessageEntity(
                    msgTag = TcpMsgTag.setDeviceIPAndPort,
                    bodyObject = radarTcpServer as Object
                )
            )
            it.offer(TcpMessageEntity(msgTag = TcpMsgTag.getHardwareVersion))
            it.offer(messageEntity)
        }

    }

    /**初始化ble消息队列*/
    private inline fun initQueue(ssid: String?, passWord: String?) {
        mRun = true
        bleConcurrentLinkedQueue = ConcurrentLinkedQueue<BleMsgEntity>()
        bleConcurrentLinkedQueue?.let {
            it.clear()
            it.offer(BleMsgEntity(bleMsgTag = BleMsgTag.RESET_DEVICE, radarType = deviceType))
            it.offer(
                BleMsgEntity(
                    bleMsgTag = if (deviceType === DeviceType.ASSURE) BleMsgTag.GET_ASSURE_DEVICE_ID else BleMsgTag.GET_DEVICE_ID,
                    radarType = deviceType
                )
            )
            it.offer(
                BleMsgEntity(
                    bleMsgTag = BleMsgTag.SET_NETWORK_INFO,
                    deviceType,
                    "$ssid,$passWord"
                )
            )
            it.offer(
                BleMsgEntity(
                    bleMsgTag = BleMsgTag.SET_SERVER_INFO,
                    deviceType,
                    radarTcpServer
                )
            )
            it.offer(BleMsgEntity(bleMsgTag = BleMsgTag.CLOSE_BLUETOOTH, deviceType, "cb,"))
        }
    }

    /**消除队首元素*/
    private inline fun executed() {
        if (deviceType?.deviceConnectType == DeviceConnectType.TYPE_WIFI && tcpConcurrentLinkedQueue != null) {
            tcpConcurrentLinkedQueue?.poll()
        } else {
            bleConcurrentLinkedQueue?.poll()
        }
    }

    fun executed(entity: BleMsgEntity) {
        if (deviceType?.deviceConnectType != DeviceConnectType.TYPE_WIFI) {
            //软重启命令只发一次，这里不需要移除
            if (null != bleConcurrentLinkedQueue && entity.bleMsgTag !== BleMsgTag.RESET_DEVICE) {
                bleConcurrentLinkedQueue.poll()
            }
        }
    }


    /**发送消息*/
    private inline fun sendMessageQueue() {
        AppExecutors.cpuIO.execute {
            when (deviceType?.deviceConnectType) {
                DeviceConnectType.TYPE_WIFI -> {
                    while (mRun && tcpConcurrentLinkedQueue?.isEmpty() == false) {
                        try {
                            TimeUnit.MILLISECONDS.sleep(DELAY)
                            val tcpMessageEntity: TcpMessageEntity = tcpConcurrentLinkedQueue.peek()
                            tcpMessageEntity?.let {
                                deviceType.getDeviceConnectInstance().sendMsg(it)
                            }
                        } catch (e: InterruptedException) {
                            e.printStackTrace()
                        }
                    }
                }


                DeviceConnectType.TYPE_BLE -> {
                    while (mRun && bleConcurrentLinkedQueue?.isEmpty() == false) {
                        try {
                            TimeUnit.MILLISECONDS.sleep(DELAY)
                            val bleMessageEntity: BleMsgEntity = bleConcurrentLinkedQueue.peek()
                            if (null != bleMessageEntity) {
                                LogUtil.d("发送的命令：${bleMessageEntity.bleMsgTag}")
                                deviceType.getDeviceConnectInstance().sendMsg(bleMessageEntity)
                                //软重启命令只发一次，发完就移除
                                if (bleMessageEntity.bleMsgTag === BleMsgTag.RESET_DEVICE) {
                                    bleConcurrentLinkedQueue.poll()
                                } else if (bleMessageEntity.bleMsgTag === BleMsgTag.CLOSE_BLUETOOTH) {
                                    bleConcurrentLinkedQueue.poll()
                                }
                            }
                        } catch (e: InterruptedException) {
                            e.printStackTrace()
                        }
                    }
                }

                else -> {}
            }
        }
    }

    /**设备连接
     * 公用方法*/
    fun deviceConnect() {
        when (deviceType?.deviceConnectType) {
            DeviceConnectType.TYPE_WIFI -> {
                if (WifiUtils.getInstance(context as Context).getWifiSSID().isNullOrEmpty()) {
                    launchUIThread {
                        iDeviceStatusCallback?.callBackDeviceStatus(STATUS_WIFI_NO_CONNECT)
                    }
                    return
                }
            }

            DeviceConnectType.TYPE_BLE -> {
                (deviceType.getDeviceConnectInstance() as BleClient).setDeviceName(deviceType.getDeviceNameByString())
            }

            else -> {}
        }

        /*连接设备*/
        deviceType?.getDeviceConnectInstance()?.connect()
    }


    fun linkedQueueIsEmpty(): Boolean {
        if (deviceType?.deviceConnectType == DeviceConnectType.TYPE_WIFI) {
            if (null != tcpConcurrentLinkedQueue) {
                return tcpConcurrentLinkedQueue.isEmpty()
            }
        } else {
            if (null != bleConcurrentLinkedQueue) {
                return bleConcurrentLinkedQueue.isEmpty()
            }
        }
        return true
    }


    /**停止发消息*/
    fun stopSend() {
        mRun = false
    }


    /**在UI线程通知*/
    private fun launchUIThread(runnable: Runnable?) = context?.runOnUiThread(runnable)

    /**释放资源 */
    fun close() {
        deviceType?.iDeviceConnect?.close()
    }


}