package com.axend.radarcommandsdk.connect.bean

import com.axend.radarcommandsdk.R
import com.axend.radarcommandsdk.connect.DeviceNetworkConnect.Companion.applicationContext
import com.axend.radarcommandsdk.connect.ble.BleClient
import com.axend.radarcommandsdk.connect.contract.IDeviceConnect
import com.axend.radarcommandsdk.connect.tcp.NettyClient

/**
 * @description设备类型
 * @author admins
 * @time 2023/12/18 18:01
 */
enum class DeviceType(
    private val deviceType: Int,
    val deviceName: Int,
    val deviceConnectType: DeviceConnectType,
    val iDeviceConnect: IDeviceConnect,
    val magicHead: Byte
) {
    /**防摔*/
    ASSURE(
        0,
        R.string.common_aerosense_assure,
        DeviceConnectType.TYPE_BLE,
        BleClient.instance,
        0x00
    ),

    /**呼吸心率*/
    WAAVE(1, R.string.common_aerosense_wavve, DeviceConnectType.TYPE_BLE, BleClient.instance, 0x13),

    /**老版本防摔 使用tcp配网方式*/
    ASSURE_OLD(
        4,
        R.string.common_aerosense_assure,
        DeviceConnectType.TYPE_WIFI,
        NettyClient.instance,
        0x00
    );


    companion object {
        var typeMap: HashMap<Int, DeviceType?> = hashMapOf()
        var nameMap: HashMap<String?, DeviceType?> = hashMapOf()
        var magicMap: HashMap<Byte, DeviceType?> = hashMapOf()

        init {
            for (r in DeviceType.values()) {
                typeMap[r.getType()] = r
                nameMap[r.getDeviceNameByString()] = r
                magicMap[r.getMagic()] = r
            }
        }

        fun getMagic(magicHead: Byte): DeviceType? {
            return magicMap[magicHead]
        }
    }

    fun getDeviceConnectInstance(): IDeviceConnect {
        return iDeviceConnect
    }


    /**获取设备类型*/
    private fun getType(): Int {
        return deviceType
    }


    /**获取设备名称字符*/
    fun getDeviceNameByString(): String? {
        return applicationContext?.getString(deviceName)
    }

    private fun getMagic(): Byte {
        return magicHead
    }

    open fun getMagicForMap(magicHead: Byte): DeviceType? {
        return magicMap[magicHead]
    }


}