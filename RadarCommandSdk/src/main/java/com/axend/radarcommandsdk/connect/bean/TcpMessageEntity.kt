package com.axend.radarcommandsdk.connect.bean

/**
 * @description Tcp传输数据类
 * @author admins
 * @time 2023/12/21 15:43
*/
data class TcpMessageEntity(
    var msgTag: TcpMsgTag? = null,
    var length: Int? = null,
    var body: ByteArray? = null,
    var bodyObject: Object? = null
) {
    companion object {
        /*头部规范*/
        val messageHeader: ByteArray = byteArrayOf(0xAA.toByte(), 0xAA.toByte(), 0x55, 0x55)
    }


}
