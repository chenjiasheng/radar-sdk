package com.axend.radarcommandsdk.connect.bean

import com.axend.radarcommandsdk.utils.LogUtil

/**
 * @description 指令集枚举类
 * @author admins
 * @time 2023/12/19 16:39
 */
enum class TcpMsgTag(private val tag: Int, private val bytes: ByteArray) {
    getID(2, byteArrayOf(0x00, 0x02)),  //获取雷达ID
    setWorkDistance(3, byteArrayOf(0x00, 0x03)),  //设置工作距离
    getWorkDistance(4, byteArrayOf(0x00, 0x04)),  //获取工作距离
    setAlarmDelay(5, byteArrayOf(0x00, 0x05)),  //设置报警延时
    getAlarmDelay(6, byteArrayOf(0x00, 0x06)),  //获取报警延时
    switchSTAMode(7, byteArrayOf(0x00, 0x07)),  //切换到STA模式
    setFamilyWifiInfo(10, byteArrayOf(0x00, 0x0A)),  //设置家庭Wifi信息
    getHardwareVersion(11, byteArrayOf(0x00, 0x0B)),  //获取设备固件版本信息
    setDeviceIPAndPort(12, byteArrayOf(0x00, 0x0C)),  //设置IP与端口
    setDeviceHeight(25, byteArrayOf(0x00, 0x19)),  //设置设备安装高度
    getDeviceHeight(26, byteArrayOf(0x00, 0x1A)), ;//获取设备安装高度

    companion object{
         private var map: Map<Int, TcpMsgTag>? = mutableMapOf()

        fun get(tag: Int): TcpMsgTag?{
            for (msg in TcpMsgTag.values()) {
                (map as MutableMap<Int, TcpMsgTag>)[msg.getRadarCommandTag()] = msg
            }
//            LogUtil.d("${map}")
//            LogUtil.d("${map?.get(2)}")
            return map?.get(tag)
        }

    }


    fun getRadarCommandByte(): ByteArray {
        return bytes
    }

    fun getRadarCommandTag(): Int {
        return tag
    }


}