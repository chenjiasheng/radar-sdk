package com.axend.radarcommandsdk.connect.bean

/**
 * @description蓝牙指令
 * @author admins
 * @time 2024/1/19 15:38
 */
enum class BleMsgTag(
    private val type: Int,
    private val command: Short,
    private val tagName: String
) {
    SET_STATUS(-1, 0x887,"设置参数状态（成功/失败）"),

    //IP和端口格式：[ip,port]
    SET_SERVER_INFO(1, 0x889,"设置服务器IP和端口"),

    //WIFI账号和密码格式：(ssid,password)
    SET_NETWORK_INFO(2, 0x880,"设置WIFI账号和密码"),


    GET_DEVICE_ID(3, 0x0410, "获取设备ID"),
    RESET_DEVICE(4, 0x0411, "重启设备"),
    GET_ASSURE_DEVICE_ID(5, 0x00, "获取Assure设备ID"),

    CLOSE_BLUETOOTH(9, "关闭蓝牙命令"),
    ;


    constructor(type: Int, tagName: String) : this(type, 0, tagName)

    companion object {
        fun fromTag(tag: Short): BleMsgTag? {
            return BleMsgTag.values().firstOrNull { it.command == tag }
        }
    }


}