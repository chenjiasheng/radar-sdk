package com.axend.radarcommandsdk.constant

/**
 * 常量集（顶层）
 */

/*连接状态值*/
const val STATUS_SUCCESS = 11 //设备连接成功
const val STATUS_FAILED = 12 //设备连接失败
const val STATUS_BROKEN = 13 //设备连接中断

const val STATUS_WIFI_NO_CONNECT = 14//wifi未连接
const val STATUS_COMMAND_COMPLETE = 15//命令发送完成


